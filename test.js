export default class SpacesConfigurationModel {
    constructor({ currentSpaceId, currentUser }) {
        this.currentSpaceId = currentSpaceId;
        this.currentUser = currentUser;
        this.space = null;
        this.users = [];
        this.isOwner = false;
    }

    setSpaceAndUsers(space) {
        this.space = space;
        this.users = space.users;
    }

    getMaxSize() {
        return this.space && this.space.maxSize;
    }

    isSandboxSpace() {
        return this.space && this.space.isSandbox;
    }

    setOwnership() {
        this.isOwner = this.space.users.some(user => user.user._id === this.currentUser._id && user.isOwner === true);
    }

    addUser(userList) {
        this.users = [...this.users, ...userList];
    }

    removeUser(userId) {
        this.users = this.users.filter(user => user.user._id !== userId);
    }

    getUserMailsInSpace() {
        return this.users.map(({user}) => user.mail);
    }

    getUserInSpaceCount() {
        return this.users.length;
    }

    getAvailableSlots() {
        return this.space.maxSize - this.getUserInSpaceCount();
    }

    switchOwnership(user) {
        user.isOwner = !user.isOwner;
    }
}
